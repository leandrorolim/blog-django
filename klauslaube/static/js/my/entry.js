define("my/entry", [
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/query",
    "dojo/on",
    "prettify"
], function(lang, dom, query, on, common) {

    return {
        init: function() {
            // Prettify the pre content
            prettyPrint();

            // Search by #respond, when click it, focus the first field in
            // response form.
            this.postDetail = query('.post-detail')[0];
            on(this.postDetail, "a[href='#respond']:click",
                lang.hitch(this, "onClickRespond"));
        },

        // Events
        onClickRespond: function(e) {
            e.preventDefault();
            query("#id_name", this.postDetail)[0].focus();
        }
    };

});
