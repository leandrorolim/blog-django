from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.comments.moderation import moderator, AlreadyModerated
from django.db.models import signals

from diario.models import Entry
from diario.sitemaps import DiarioSitemap

from core.feeds import DiarioRssEntriesFeed, DiarioRssEntriesByTagFeed
from core.signals import replace_slideshare_shortcode, EntryModerator
from portfolio.sitemaps import PortfolioSitemap

# Add shortcode to pre_save signal of entry
signals.pre_save.connect(replace_slideshare_shortcode, sender=Entry)
# Let the system send an email when someone comments
try:
    moderator.register(Entry, EntryModerator)
except AlreadyModerated:
    pass

# Autodiscover admin
admin.autodiscover()

# Sitemaps settings
sitemaps = {
    'blog': DiarioSitemap,
    'portfolio': PortfolioSitemap,
}

# URL settings
handler500 = 'views.server_error'

urlpatterns = patterns('',
    # Blog feeds
    url(r'^feed/$', DiarioRssEntriesFeed(), name='diario_feed'),
    url(r'^tag/(?P<tag_name>[^/]+)/feed/$', DiarioRssEntriesByTagFeed(),
        name='diario_tag_feed'),

    # Blog urls
    url(r'', include('diario.urls.entries')),
    url(r'^tag/', include('diario.urls.tagged')),
    url(r'^comentarios/', include('django.contrib.comments.urls')),

    url(r'^portfolio/', include('portfolio.urls', namespace='portfolio')),
    url(r'^contato/', include('simple_contact.urls')),

    # Search urls
    url(r'^busca/', include('haystack.urls')),

    # Sitemap
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {
        'sitemaps': sitemaps
    }, name='sitemap'),

    # Admin urls
    url(r'^admin/media_manager/', include('media_manager.urls',
        namespace='media_manager')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.LOCAL:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
