# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from portfolio.models import Customer, Job


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'website', 'is_active', )
    search_fields = ('name', 'description', )
    prepopulated_fields = {'slug': ('name', )}

    fieldsets = (
        (None, {
            'fields': ('name', 'slug', 'description', 'website',
                'is_active', ),
        }),
    )


class JobAdmin(admin.ModelAdmin):
    list_display = ('description', 'customer', 'creation_date', 'is_active', )
    search_fields = ('description', 'details_source', )
    list_filter = ('customer', )
    prepopulated_fields = {'slug': ('description', )}

    fieldsets = (
        (None, {
            'fields': ('customer', 'description', 'slug', 'details_source',
                'href', ),
        }),
        (_(u'Avançado'), {
            'fields': ('sample_image', 'my_role', 'working_regime',
                'i_used_the', 'creation_date', 'tags', 'is_active', ),
        }),
    )


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Job, JobAdmin)
