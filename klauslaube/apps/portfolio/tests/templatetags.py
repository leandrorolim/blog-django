from django.test import TestCase
from portfolio.templatetags.portfolio_extras import job_list


class PortfolioTemplatetagsTests(TestCase):
    fixtures = ['portfolio_testdata.json', ]

    def test_job_list(self):
        context = job_list()
        self.assertEquals(len(context['jobs']), 3)
