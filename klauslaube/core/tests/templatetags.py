from django.test import TestCase
from django.test.client import RequestFactory

from core.templatetags.core_extras import current, full_content, readmore


class CoreTemplateTagsTests(TestCase):
    def setUp(self):
        super(CoreTemplateTagsTests, self).setUp()

        self.paragraphs = (
            '<p>First Paragraph!</p>',
            '<section><div><p><!-- readmore --></p></div></section>',
            '<p>Second Paragraph!</p>',
        )
        self.content = ''.join(self.paragraphs)

    def test_current_link(self):
        """
        Assert that the method is matching the pattern.
        """
        factory = RequestFactory()
        request = factory.get('/blog/')

        # Match an item
        class_ = current(request.path, '^/blog/$')
        self.assertEquals(class_, 'current')

        # Don't match
        class_ = current(request.path, '^/portfolio/$')
        self.assertFalse(class_)

    def test_readmore(self):
        """
        Assert that the response is equals to the first part of the
        text.
        """
        response = readmore(self.content)
        self.assertEquals(response, self.paragraphs[0])

    def test_full_content(self):
        """
        Assert that the method is replacing the readmore pattern
        to a anchor.
        """
        response = full_content(self.content)
        self.assertTrue('<span id="readmore"></span>' in response)
